%define next 0

%macro colon 2
    %ifstr %1
	    %ifid %2
	        %%link: dq next
	        %%key: db %1, 0
	        %2:
	            %define next %%link
	    %else
	        %error "Second arg should be key"
	    %endif
    %else
	    %error "First arg should be string"
    %endif
%endmacro
