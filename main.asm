%include "colon.inc"
%include "words.inc"
%include "lib.inc"
%define buffer_size 255

global _start

extern find_word

section .rodata
value: db "Value: ", 10, 0
no_value: db "Key is missing", 10, 0
too_long: db "Key is too long", 10, 0

section .bss
buffer: resb buffer_size

section .text
_start:
    .loop:
        sub rsp, buffer_size 
        mov rsi, buffer_size 
        mov rdi, rsp 
        call read_word 
        test rax, rax
        jz .message_too_long
        mov rdi, rax 
        mov rsi, next 
        call find_word 
        test rax, rax
        jz .message_no_value 

        add rsp, buffer_size 
        add rax, 8 
        push rax 
        mov rdi, value
        call print_string
    
        pop rdi 
        push rdi 
        call string_length 
        pop rdi
        inc rax 
        add rdi, rax
        call print_string 
        call print_newline
	
        call exit

    .message_no_value:
        add rsp, buffer_size
        mov rdi, no_value
        call print_error
     
    .message_too_long:
        add rsp, buffer_size
        mov rdi, too_long
        call print_error
