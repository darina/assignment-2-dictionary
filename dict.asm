global find_word
%include "lib.inc"

%define NUM 8

section .text
find_word:
    .loop:
    	test rsi, rsi 
    	je .stop
    	add rsi, NUM
		push rsi
		push rdi
    	call string_equals
		pop rdi
		pop rsi
    	sub rsi, NUM 
		test rax, rax
    	jnz .result 
    	mov rsi, [rsi] 
    	jmp .loop
    
    .stop:
        xor rax, rax
        ret

    .result:
        mov rax, rsi
        ret
