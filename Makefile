ASM_FLAGS=-f elf64
ASM=nasm
LD=ld


%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<

build: main.o lib.o dict.o
	$(LD) -o program $?

clear:
	$(RM) program *.o

run: build
	./program

.PHONY: build run clear
