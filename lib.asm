global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error

%define STDOUT_FD 1
%define STDERR_FD 2

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0  ; если не 0, инкрементируем rax
        je .end
        inc rax
        jmp .loop
    .end:
        ret

print_error:
    push rdi
    push rsi
    call string_length 
    pop rsi
    pop rdi
    mov rsi, rdi 
    mov rdx, rax 
    mov rax, 1 
    mov rdi, STDERR_FD ; stderr 
    syscall
    jmp exit

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    mov rsi, rdi
    mov rdi, STDOUT_FD
    mov rdx, rax
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, STDOUT_FD
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rdi

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char 
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r10, 10
    push 0 ; stop word
    .loop:
	    mov rdx, 0
	    div r10
	    add rdx, '0'
	    push rdx
        cmp rax, 0
	    je .print
	    jmp .loop
        .print:
	        pop rdi
	        cmp rdi, 0
	        je .end
	        call print_char
	        jmp .print
    .end:
	    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax 
    xor rcx, rcx
    .loop:
        mov r13b, byte[rdi+rcx] 
        mov r12b, byte[rsi+rcx] 
        inc rcx 
        cmp r13, r12 
        je .check_null 
        ret 
    .check_null:
        test r13, r12 
        jnz .loop
        inc rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    push rax
    mov rdi, 0
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi
    push rsi
    .check_spaces:
        call read_char
        cmp rax, 0x20
        je .check_spaces
        cmp rax, 0x9
        je .check_spaces
        cmp rax, 0xa
        je .check_spaces
        pop r11 
        pop r10 
        push r12
        xor r12, r12   
    .read_word:
        cmp r11, r12
        je .buffer_overflow
        mov [r10+r12], rax

	cmp rax, 0
        je .end
        cmp rax, 0x20
        je .end
        cmp rax, 0x9
        je .end
        cmp rax, 0xa
        je .end
        inc r12
        push r10
        push r11
        call read_char
        pop r11
        pop r10
        jmp .read_word      
    .end:
        mov byte[r10+r12], 0
        mov rax, r10
        mov rdx, r12
        pop r12
        ret 
    .buffer_overflow:
        pop r12
        mov rax, 0
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0
    mov rdx, 0
    push r10
    mov r10, 0
    mov r10b, byte[rdi]
    cmp r10b, '0'
    jb .error
    cmp r10b, '9'
    ja .error
    sub r10b, '0'
    mov al, r10b
    mov rdx, 1
    .loop:
	    mov r10b, byte[rdi+rdx]
	    cmp r10b, '0'
	    jb .end
	    cmp r10b, '9'
	    ja .end
	    inc rdx
	    imul rax, 0xA
	    sub r10b, '0'
	    add rax, r10
	    jmp .loop
    .error:
	    pop r10
	    mov rax, 0
	    mov rdx, 0
	    ret
    .end:
	    pop r10
	    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov rax, 0
    mov rdx, 0
    cmp byte[rdi], '-'
    je .minus
    jmp parse_uint
    .minus:
	    inc rdi
	    call parse_uint
	    inc rdx
	    neg rax
	    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r10, r10
    call string_length
    add rax, 1
    cmp rdx, rax ; size checking
    jl .fail
    .loop:
	    mov r10b, byte[rdi]
	    mov byte[rsi], r10b
	    cmp r10, 0
	    je .end
	    inc rax
	    inc rsi
	    inc rdi
	    jmp .loop
    .fail:
	    mov rax, 0
	    ret
    .end:
	    ret
